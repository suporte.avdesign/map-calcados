# Comandos GIT

## Desfazer último commit
**Caso o commit não tenha sido publicado, pode-se desfazer o último commit atraves do comando**

*Elimina as atividades feitas no stage:*

````
git reset HEAD~1 --hard
````
*Voltar com as atividades ao stage:*

````
git reset HEAD~1 --soft
````
**Se já tiver feito push, o melhor é "reverter".**

*Neste contexto, significa criar um commit novo que apague as linhas introduzidas, introduza as linhas apagadas no último commit.*

````
git revert HEAD~1
````

*Ou HEAD~2 para reverter os 2 últimos commits.*



````
comando aqui
````
````
comando aqui
````

````
comando aqui
````