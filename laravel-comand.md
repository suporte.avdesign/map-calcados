# Artisan

## Models
`php artisan make:model NomeModel -m`


## Controllers
`php artisan make:controller Api\\NomeController`

`php artisan make:controller Api\\NomeController --api`


## Resources
`php artisan make:resource NomeResource`


## Requests
`php artisan make:request NomeRequest`


## Observvers 
`php artisan make:observer NomeObserver --model=NomeModel`